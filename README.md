# Automated Test Suites. Why automation? 

![](https://thumbs.dreamstime.com/z/illustration-different-types-test-automation-framework-which-set-protocols-rules-standards-guidelines-like-90426213.jpg)

## Abstracts:
-----
-  This paper is about Automated Test Suites(ATS) which allows you to execute software test cases using an automated testing tool and then,  compares the actual results with the expected results and I have explained what is Automated Test Suites, Why automation,Automation          Benefits,levels of automation testing process and Automation Testing Tools .Moreover i have included the basic fundamentals of automation.


## Table of Contents :
---

  1. [Introduction](#introduction)

  2. [Why automation?](#Why automation?)

  3. [ Automation Benefits](#Test Automation Benefits)

  4. [Levels of automation testing](#Levels of automation testing)
  
  5. [Automation Testing Tools] (#Automation Testing Tools)
      
  6. [Conclusions](#conclusion) 


---
## Introduction 
---
  
  - Automated Test Suites(ATS)  allows you to execute software test cases using an automated testing tool or writing test scripts and then, compares the actual results with the expected results.
  
  - Automated Test Suites is built using Oracle Linux 7-slim as the base image.
  
  - Through Automated Testing Suite (ATS), Oracle Communications aims at providing an end-to-end solution to its customers for deploying and testing its 5G-NFs.


  ---
## Why automation
  ---
  - Automation can keep your process in-house, improve process control and significantly reduce lead times compared to outsourcing or going overseas.
  
  - Automation solutions are based on your unique needs and goals and pay for themselves quickly due to lower operating costs, reduced lead times, increased output and more.
  
  - Shared automated tests can be used by developers to catch problems quickly before sending to QA . Tests can run automatically whenever source code changes are checked in and notify the team or the developer if they fail.
  
  - Automated testing is capable of running thousands of tests simultaneously, simulating millions of users, all of which is next to impossible with manual testing.

---
## Automation Benefits
---
 - Reduce employee workload, improve productivity.
 
 - Fast Development and Delivery.
 
 -  More Accurate Tests.
  
 ---
## Levels of test automation
 ---
###### Theree levels of test automation 
 
 1. Unit Test Level:- At this initial level, that is the foundation of test automation pyramid, automated unit tests or component / module tests are written by developers.
 
 2. Functional Tests Level (known as the service layer):- As a rule, it is impossible to accurately test business logic layer of an application’s architecture. This may be due to the fact that business logic was implemented so as not to be exploited by the user. 
 
 3. UI Tests Level :- At this level, there is the possibility to test both user interface and functionality by performing operations that stimulate business logic of the app.
 
 - To improve quality of the product, specialists recommend you to automate tests at these 3 different levels

  ----
## Automation Testing Tools
  ---
  
  1. Katalon - Katalon is a test automation tool based on the automation frameworks of Selenium and Appium.
  
  2. Selenium - Selenium is a popular open-source (released under the Apache License 2.0) automation testing framework.
   
  3. LambdaTest - LambdaTest is a cloud-based automation testing tool for desktop and mobile applications
  
---
## Conclusions
---

- Automation testing consists of a Software testing technique. This technique uses a software tool to test other software
Moreover, Automation Testing can provide very detailed and insightful reports. Commonly, development cycles require the same test to be executed several times. When using a test automation tool, users can record the test suit and run it whenever required, without being dependent on human interference. However, in most cases, the aim is not to entirely discard Manual Testing. Automation testing is the process of testing software and other tech products to ensure it meets strict requirements.

---
## References :
---
[Javatpoint](https://www.javatpoint.com/automation-testing-tool)

[Tutorialspoint](https://www.tutorialspoint.com/automation-testing-tutorial-for-beginners-process-benefits-tools)
